import React from "react";
import HomePage from "./src/homePage";
import HomeScreen from "./src/loginScreen";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} options={{ title: "Login" }} />
        <Stack.Screen name="HomePage" component={HomePage} options={{ title: "Classroom Reservation" }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
