import React, { useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicker = ({ type, buttonTitle, dateKey, setSchedule }) => {
  // Parâmetros necessários
  const [DatePickerVisible, setDatePickerVisible] = useState(false); // Estado para controlar a visibilidade do Modal

  const showDatePicker = () => {
    setDatePickerVisible(true); // Função para exibir calendário
  }; // Fim da const showDatePicker

  const hideDatePicker = () => {
    setDatePickerVisible(false); // Função para esconder o calendário - Modal
  }; // Fim da const hideDatePicker

  const handleConfirm = (date) => {
    if (type === "time") {
      // Lógica para extrair hora e minuto
      const hour = date.getHours(); // Função nativa tipo date
      const minute = date.getMinutes(); // Obtém os minutos da hora

      // Lógica para montar hora e minuto no formato desejado
      const formattedTime = `${hour}:${minute}`;

      // Atualiza o estado da reserva com HH:mm formatada
      setSchedule((prevState) => ({
        // prevState - mantém o estado anterior
        ...prevState,
        [dateKey]: formattedTime, // Utiliza o dateKey para atualizar a data no estado com o novo horário formatado
      })); // Fim da setSchedule
    } // Fim do if
    else {
      // Atualizar Schedule
      const formattedDate = date.toISOString().split('T')[0];
      setSchedule((prevState) => ({
        // prevState - mantém o estado anterior
        ...prevState,
        [dateKey]: formattedDate, // Utiliza o dateKey para atualizar a data no estado
      })); // Fim do setSchedule
    } // Fim do else
  }; // Fim da const handleConfirm

  return (
    <View>
      {/* botão interativo, aciona o calendário quando ele for true, por meio da função onPress */}
      <Button title={buttonTitle} onPress={showDatePicker} color="black" />
      {/* Componente de Modal do calendário com suas propriedades configuradas */}
      <DateTimePickerModal
        isVisible={DatePickerVisible} // Controla a visibilidade do Modal
        mode={type} // Define se é para selecinar data ou hora
        locale="pt_BR" // Define a localidade
        onConfirm={handleConfirm} // Função chamada quando a data / hora é confirmada
        onCancel={hideDatePicker} // Função chamada quando o modal é cancelado

        // Estilo opicinal
        //pickerComponentStyleIOS={{backgroundColor:"#fff"}}
        //textColor="#000"
      />
      {/* Fechamento do DateTimePickerModal */}
    </View> // Fechamento da View
  ); // Fim do return
}; // Fim da const principal

export default DateTimePicker;
