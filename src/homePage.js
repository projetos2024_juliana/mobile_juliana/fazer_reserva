import api from "./axios/axios";
import CheckDays from "./components/checkDays";
import React, { useState, useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import DateTimePicker from "./components/datePicker";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Button,
  Modal,
  Alert,
} from "react-native";

const HomePage = ({ route }) => {
  const { user, nameUser } = route.params; // Dados do Usuário após Login
  const [classrooms, setClassrooms] = useState([]); // Estado para armazenar as salas de aula
  const [showModal, setShowModal] = useState(false); // Estado para controlar a visibilidade do modal
  const [scheduleDefault, setScheduleDefault] = useState({
    dateStart: "", // Data de início da reserva
    timeStart: "", // Hora de início da reserva
    dateEnd: "", // Data de término da reserva
    timeEnd: "", // Hora de término da reserva
    days: [], // Dias selecionados para a reserva
    user: user, // Esse deve pegar o User autenticado
    classroom: "", // Esse não precisa ser preenchida
  });

  const [schedule, setSchedule] = useState(scheduleDefault); // Estado para armazenar os detalhes da reserva
  const navigation = useNavigation(); // Navegação do React

  useEffect(() => {
    // useEffect para buscar as salas de aula
    const fetchClassrooms = async () => {
      try {
        const response = await api.getAllClassroom(); // Chamada API
        setClassrooms(response.data.classrooms); // Atualiza o estado com as salas obtidas
      } catch (error) {
        console.error("Erro ao obter salas:", error); // Log do erro em caso de falha
      }
    };

    fetchClassrooms();
  }, []);

  const handleReservation = (classroomNumber) => {
    //  Atualizar o número da sala
    setSchedule({
      ...scheduleDefault,
      classroom: classroomNumber,
    }); // Fim da setSchedule

    // Abre o modal
    setShowModal(true);
  }; // Fim da const handleReservation

  const createSchedule = async () => {
    console.log(schedule);

    await api
      .createSchedule(schedule)
      .then((response) => {
        Alert.alert("Reserva criada com sucesso", response.data.message);

        setShowModal(false);
        setSchedule(scheduleDefault);
      })
      .catch((error) => {
        Alert.alert("Erro", error.response.data.error);
      }); // Fim do await
  }; // Fim do createSchedule

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{nameUser}</Text>
      <FlatList
        data={classrooms}
        keyExtractor={(item) => item.number.toString()}
        renderItem={({ item }) => (
          <View style={styles.item}>
            <Text style={styles.itemTitle}>Sala: {item.number}</Text>
            <Text style={styles.itemText}>Descrição: {item.description}</Text>
            <Text style={styles.itemText}>Capacidade: {item.capacity}</Text>
            <View style={styles.buttonContainer}>
              <Button
                title="Reservar"
                onPress={()=> handleReservation(item.number)}
                color="blue"
              />
            </View>
          </View>
        )}
      />

      {/* Modal */}
      <Modal visible={showModal} animationType="slide" transparent={true}>
        <View style={styles.modalBackground}>
          <View style={styles.modalContainer} />
          <Text style={styles.modalTitle}>Formulários de Reservas:</Text>

          <DateTimePicker
            type={"date"}
            buttonTitle={
              schedule.dateStart === ""
                ? "Data de Início"
                : schedule.dateStart.toLocaleString()
            }
            setSchedule={setSchedule}
            dateKey={"dateStart"}
          />

          <DateTimePicker
            type={"date"}
            buttonTitle={
              schedule.dateEnd === ""
                ? "Data de Fim"
                : schedule.dateEnd.toLocaleString()
            }
            setSchedule={setSchedule}
            dateKey={"dateEnd"}
          />

          <CheckDays
            selectedDays={schedule.days}
            setSchedule={setSchedule}
          ></CheckDays>

          <DateTimePicker
            type={"time"}
            buttonTitle={
              schedule.timeStart === ""
                ? "Início do Período"
                : schedule.timeStart.toLocaleString()
            }
            setSchedule={setSchedule}
            dateKey={"timeStart"}
          />

          <DateTimePicker
            type={"time"}
            buttonTitle={
              schedule.timeEnd === ""
                ? "Fim do Perído"
                : schedule.timeEnd.toLocaleString()
            }
            setSchedule={setSchedule}
            dateKey={"timeEnd"}
          />

          <Button
            title={"Reservar " + schedule.classroom}
            color="green"
            onPress={createSchedule}
          />
          <Button
            title="Cancelar"
            color="red"
            onPress={() => setShowModal(false)}
          />

          <View></View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 10,
    backgroundColor: "#084d6e",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: "center",
    color: "#fff",
  },
  item: {
    backgroundColor: "#fff",
    marginBottom: 20,
    padding: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
  },
  itemTitle: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
  itemText: {
    fontSize: 16,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Define um fundo semi-transparente
    justifyContent: "center",
    alignItems: "center",
  },
  modalContainer: {
    backgroundColor: "#084d6e",
    borderRadius: 10,
    padding: 20,
    width: "80%", // Define a largura do modal
    maxHeight: "70%", // Define a altura máxima do modal
    justifyContent: "center",
    alignItems: "center",
  },
  modalTitle: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    color: "#fff",
  },
  input: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    marginBottom: 10,
    padding: 10,
    width: "100%",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  picker: {
    width: "100%",
    marginBottom: 10,
  },
});

export default HomePage;
